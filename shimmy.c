#include <stdio.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>

#define MAX_IMU_NUMBER 8

int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf("error %d from tcgetattr", errno);
                return -1;
        }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                printf("error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}
void set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                printf("error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                printf("error %d setting term attributes", errno);
}

char* getLocalTimeString(char* timeDateString)
{
	time_t t;
	struct tm* actualTime;
	time(&t);
	actualTime= localtime(&t);
	sprintf(timeDateString,"_%d-%d-%d_%d_%d_%d", actualTime->tm_year + 1900, actualTime->tm_mon + 1, actualTime->tm_mday, actualTime->tm_hour, actualTime->tm_min, actualTime->tm_sec);
	return timeDateString;
}

double evaluateElapsedTime(struct timeval* startingTime)
{
	struct timeval actualTime;
	double elapsedTime;
	gettimeofday(&actualTime, NULL);

    // compute and print the elapsed time in millisec
    elapsedTime = (actualTime.tv_sec -startingTime->tv_sec) * 1000.0;      // sec to ms
    elapsedTime += (actualTime.tv_usec - startingTime->tv_usec) / 1000.0;   // us to ms
    return elapsedTime;
}
int main (int argc, char *argv[])
{
	struct timeval t1;
	double elapsedTime;
	int numberOfIMU=1;
	char filePrefix[100] = "acquisition";
	char timeDateString[30];
	FILE *genuinoIMU[MAX_IMU_NUMBER];
	// read arguments of the function call
	if (argc > 1) // arguments were passed
  {
     for (int argumentParser = 1;argumentParser<argc;argumentParser++)
     {
     	if ((strcmp("-h", argv[argumentParser]) == 0))
     	{
     		printf("\n\n-h: for help \n-filePrefix: to change the file prefix \n-numberOfIMU: to change the number of used IMU");	
     		break;
     	}
     	if ((strcmp("-filePrefix", argv[argumentParser]) == 0)&&(argc>=argumentParser+1))
     	{
     			strcpy(filePrefix,argv[argumentParser+1]);
     			argumentParser++;
     	}
     	if ((strcmp("-numberOfIMU", argv[argumentParser]) == 0)&&(argc>=argumentParser+1))
     	{
     			numberOfIMU=atoi(argv[argumentParser+1]);
     			argumentParser++;
     			if (numberOfIMU>MAX_IMU_NUMBER)
     			{
     				perror(sprintf("error:maximum number of IMUs is: %d",MAX_IMU_NUMBER));		
     				return -1;
     			}
     	}
     }
  }
  
  for (int IMUCursor =0; IMUCursor<numberOfIMU;IMUCursor++)
  {
  	char IMUName[20];
  	sprintf(IMUName,"/dev/ttyACM%d",(int)IMUCursor);
  	genuinoIMU[IMUCursor] = fopen(IMUName,"r+b");//open for update in binary mode
  	if(genuinoIMU[IMUCursor] == NULL) 
	  {
			perror(sprintf("Error in opening Genuino 101 board number: %d",IMUCursor));
	    return(-2);
	  }
  }
	
	strcat(filePrefix,getLocalTimeString(timeDateString)); // generates file prefix name with date
	
	
	gettimeofday(&t1, NULL);
		
	
}


/*...
char *portname = "/dev/ttyUSB1"
 ...
int fd = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
if (fd < 0)
{
        error_message ("error %d opening %s: %s", errno, portname, strerror (errno));
        return;
}

set_interface_attribs (fd, B115200, 0);  // set speed to 115,200 bps, 8n1 (no parity)
set_blocking (fd, 0);                // set no blocking

write (fd, "hello!\n", 7);           // send 7 character greeting

usleep ((7 + 25) * 100);             // sleep enough to transmit the 7 plus
                                     // receive 25:  approx 100 uS per char transmit
char buf [100];
int n = read (fd, buf, sizeof buf);  // read up to 100 characters if ready to read
*/